# Build the website

## With Nix

To build the website with the flake:

```
nix build .#batsite
```

Or without the flake (using the `default.nix`):

```
nix-build default.nix  -A packages.x86_64-linux.batsite
```

It is also possible to build the site by hand:

```
nix-shell default.nix -A devShell
zola build -o /tmp
```