"""
Simple python script that converts the input file (as a bibtex file) into a zola content file.
"""

from pybtex.database.input import bibtex
import toml

md_template = """+++
title = "They use Batsim"
description = "List of Batsim-related publications."
date = 2021-05-25T18:20:00+00:00
updated = 2021-05-25T18:20:00+00:00
draft = false
weight = 421
template = "docs/publications.html"

{publication_list}
+++

# Publication list
"""

parser = bibtex.Parser()
bibdata = parser.parse_file("publications.bib")

def convert_list_to_dict(tuples):
    di = {}
    for a, b in tuples:
        di.setdefault(a, []).append(b)
    return di

# print(toml.dumps(bibdata.entries))
bib_list = []
for bib_id in bibdata.entries:
    b = bibdata.entries[bib_id].fields
    fields_map = {}
    for f in b:
        fields_map[f] = b[f]

    authors = []
    for author in bibdata.entries[bib_id].persons["author"]:
        authors.append(str(author.first_names[0]) + " " + str(author.last_names[0]))
    fields_map["author"] = ", ".join(authors)

    bib_list.append(fields_map)

publication_list = toml.dumps({ "extra": { "list": bib_list } })
print(md_template.format(publication_list = publication_list))
