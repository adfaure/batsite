+++
title = "Citing Batsim"
description = "Contributor Covenant Code of Conduct."
date = 2021-05-01T18:20:00+00:00
updated = 2021-05-01T18:20:00+00:00
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = ''
toc = false
top = false
+++

```
@inproceedings{dutot:hal-01333471,
  TITLE = {{Batsim: a Realistic Language-Independent Resources and Jobs Management Systems Simulator}},
  AUTHOR = {Dutot, Pierre-Fran{\c c}ois and Mercier, Michael and Poquet, Millian and Richard, Olivier},
  URL = {https://hal.archives-ouvertes.fr/hal-01333471},
  BOOKTITLE = {{20th Workshop on Job Scheduling Strategies for Parallel Processing}},
  ADDRESS = {Chicago, United States},
  YEAR = {2016},
  MONTH = May,
  KEYWORDS = {Reproducibility ; RJMS ; Scheduling ; Simulation},
  PDF = {https://hal.archives-ouvertes.fr/hal-01333471/file/batsim.pdf},
  HAL_ID = {hal-01333471},
  HAL_VERSION = {v1},
}
```